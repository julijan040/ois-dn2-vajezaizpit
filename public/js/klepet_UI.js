function divElementEnostavniTekst(sporocilo) {
  var tmp = sporocilo.replace(";)", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png'>");
  tmp = tmp.replace(":)", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png'>");
  tmp = tmp.replace("(y)", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png'>");
  tmp = tmp.replace(":*", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png'>");
  tmp = tmp.replace(":(", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png'>");
  //return $('<div style="font-weight: bold"></div>').text(sporocilo); vracamo HTML
  return $('<div style="font-weight: bold"></div>').html(tmp);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var globaniVzdevek;
  var globalniKanal;

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      globaniVzdevek = rezultat.vzdevek; //!!!
      //globalniKanal = rezultat.kanal; //!!!
      $('#kanal').text(globaniVzdevek + " @ " + globalniKanal); //!!!
    } else {
      sporocilo = rezultat.sporocilo;
    }

    $('#sporocila').append(divElementHtmlTekst(sporocilo));

  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    //$('#kanal').text(rezultat.kanal);
    globalniKanal = rezultat.kanal;
    //globaniVzdevek = rezultat.vzdevek;
    $('#kanal').text(globaniVzdevek + " @ " + globalniKanal); //!!!
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo.besedilo));
    //$('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});